package supportclass
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.text.ReturnKeyLabel;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	[Event(name="readCountChange", type="flash.events.Event")]
	public class FileReader extends EventDispatcher
	{
		public static const READ_COUNT_CHANGE:String = "readCountChange";
		
		private var _fileList:ArrayCollection;
		private var _readCount:uint;
		private var _isRunnig:Boolean;
		private var _fileStream:FileStream;
		
		public function FileReader(list:ArrayCollection):void
		{
			_fileList  = list;
			_fileStream = new FileStream();
			_fileStream.addEventListener(Event.COMPLETE, onComplete);
			_fileStream.addEventListener(ProgressEvent.PROGRESS, onProcess);
			_fileStream.addEventListener(IOErrorEvent.IO_ERROR, onError);
			trace("FileReader");
		}
		
		public function start():void
		{
			readCount = 0;
			isRunnig = true;
			trace("start : " + readCount);
			
			var item:FileItemVO = _fileList[readCount] as FileItemVO;
			
			if(item is FileItemVO)
			{
				trace("open file : ", item.fileName);
				_fileStream.openAsync(item.file, FileMode.READ);
			}else{
				trace("Not found logs");
			}
			
		}
		
		protected function onError(event:IOErrorEvent):void
		{
			trace("io error");
			_fileStream.removeEventListener(Event.COMPLETE, onComplete);
			_fileStream.removeEventListener(ProgressEvent.PROGRESS, onProcess);
			_fileStream.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			trace(event.errorID);
			
			if(readCount >= _fileList.length)
			{
				_fileStream.close();
				_fileStream = null;
				Alert.show(readCount + " Log file(s) read complete.");
			}else{
				read(readCount);
				readCount ++;
			}
		}
		
		protected function onProcess(event:ProgressEvent):void
		{
			if(!isRunnig)
			{
				trace("stop");
				_fileStream.close();
				_fileStream.removeEventListener(Event.COMPLETE, onComplete);
				_fileStream.removeEventListener(ProgressEvent.PROGRESS, onProcess);
				_fileStream.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			}
		}
		
		protected function onComplete(event:Event):void
		{
			_fileStream.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			_fileStream.removeEventListener(ProgressEvent.PROGRESS, onProcess);
			_fileStream.removeEventListener(Event.COMPLETE, onComplete);
			
			if(readCount >= _fileList.length)
			{
				_fileStream.close();
				_fileStream = null;
				Alert.show(readCount + " Log file(s) read complete.");
			}else{
				read(readCount);
				readCount ++;
			}
		}
		
		private function read(index:uint):void
		{
			var item:FileItemVO = _fileList[readCount];
			
			if(item is FileItemVO)
			{
				trace("read file : ", index, item.fileName);
				
				var content:String = _fileStream.readUTFBytes(_fileStream.bytesAvailable);
				_fileStream.close();
				var hosts:Array = content.match(/^\s*hostname *[a-zA-Z0-9_\-\#]+/igm);
				
				if(hosts is Array && hosts.length > 0)
				{
					item.host = hosts[0].replace(/(^\s*hostname *)([a-zA-Z0-9_\-\#]+)/ig, "$2");
				}
				
				item.userList = new ArrayCollection(content.match(/^\s*username.+/igm));
				_fileList[index] = item;
				_fileList.refresh();
				_readCount
				
				_fileStream = new FileStream();
				_fileStream.addEventListener(Event.COMPLETE, onComplete);
				_fileStream.addEventListener(ProgressEvent.PROGRESS, onProcess);
				_fileStream.addEventListener(IOErrorEvent.IO_ERROR, onError);
				_fileStream.openAsync(item.file, FileMode.READ);
			}else{
				trace("Not found logs");
			}
		}
		
		[Bindable]
		public function get isRunnig():Boolean
		{
			return _isRunnig;
		}
		
		public function set isRunnig(value:Boolean):void
		{
			_isRunnig = value;
		}
		
		[Bindable]
		public function get readCount():uint
		{
			return _readCount;
		}
		
		public function set readCount(value:uint):void
		{
			var e:Event = new Event(READ_COUNT_CHANGE);
			dispatchEvent(e);
			_readCount = value;
		}
		
		[Bindable]
		public function get fileList():ArrayCollection
		{
			return _fileList;
		}
		
		public function set fileList(value:ArrayCollection):void
		{
			_fileList = value;
		}
	}
}